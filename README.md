# MCE

MCE - Omer Shalom Application

The application show usb that the computer has. competible with different types of os.
Bonus: We have 2 views: one its the tree view and the sec it by type.

## Installation

Use the package manager [npm/yarn] to install dependencies.
You need node v14 and above installed on your machine.

Note: Its already exist in the zip file

To run the client, please open a termianl in the root folder and run this command:
```bash
npm run start-client
```

It will serve you the client static files on port 4200.
You can browse it with: http://localhost:4200/

To run the server, please open a new termianl in the root folder and run this command:
```bash
npm run start-server
```

It will run the node application via port 3001

## License
[MIT](https://choosealicense.com/licenses/mit/)