import { Component, Input, OnChanges } from '@angular/core';
import { UsbDevice } from '../models/usb-device.model';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {FlatTreeControl} from '@angular/cdk/tree';
import { FlatNode } from '../models/flat-node.model';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.css']
})
export class TreeViewComponent implements OnChanges{
  @Input() devices: UsbDevice[] = [];
  
  constructor() {}

  ngOnChanges(): void {
    if (JSON.stringify(this.dataSource.data) !== JSON.stringify(this.devices)){
      this.dataSource.data = this.devices;
    }
  }
  
  private _transformer = (node: UsbDevice, level: number) => {
    return {
      expandable: !!node.devices && node.devices.length > 0,
      name: `Product Id: ${node.productId}, Vendor Id: ${node.vendorId}`,
      level: level
    };
  };
  
  treeControl = new FlatTreeControl<FlatNode>(
    (node) => node.level,
    (node) => node.expandable
  );
  
  treeFlattener = new MatTreeFlattener(
    this._transformer,
    (node) => node.level,
    (node) => node.expandable,
    (node) => node.devices
  );
  
  dataSource = new MatTreeFlatDataSource(
    this.treeControl, this.treeFlattener);
  
  hasChild = (_: number, node: FlatNode) => node.expandable;
}
