import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, startWith, Subscription, tap } from 'rxjs';
import { UsbDevice } from './models/usb-device.model';
import { DevicesProviderService } from './services/devices.provider';
import { interval } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'MCE';
  isTreeView = false;
  timeInerval: Subscription;
  devices : UsbDevice[] = [];
  intervalSec = 10;

  constructor(private devicesProviderService: DevicesProviderService){}
  
  ngOnInit(): void{
    this.timeInerval = interval(this.intervalSec * 1000).pipe(
      startWith(0),
      map(() => this.devicesProviderService.getAllDevices()
          .pipe(
            tap(result=> this.devices = result)
          ).subscribe()),
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.timeInerval.unsubscribe();
  }
}
