import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsbDevice } from '../models/usb-device.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class DevicesProviderService {
  url = 'http://localhost:3001/api/devices';
  constructor(
    private http: HttpClient
  ) {}

  getAllDevices(): Observable<UsbDevice[]> {
    return this.http.get<UsbDevice[]>(`${this.url}`);
  }
}
