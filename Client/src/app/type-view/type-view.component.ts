import {Component, Input, OnChanges} from '@angular/core';
import { UsbDevice } from '../models/usb-device.model';

@Component({
  selector: 'app-type-view',
  templateUrl: './type-view.component.html',
  styleUrls: ['./type-view.component.css']
})
export class TypeViewComponent implements OnChanges{
  @Input() devices: UsbDevice[] = [];
  devicesChilds: UsbDevice[] = [];

  ngOnChanges(): void {
    var newDevices: UsbDevice[] = [];
    for (const currDevice of this.devices) {
      if (currDevice.devices && currDevice.devices.length > 0){
        newDevices = newDevices.concat(currDevice.devices);
      }
    }
    this.devicesChilds =newDevices;
  }
}
