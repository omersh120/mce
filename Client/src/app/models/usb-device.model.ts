export interface UsbDevice {
    vendorId: number;
    productId: string;
    type?: USBType;
    devices?: UsbDevice[];
  }

  export enum USBType {
    device = "Device",
    hub = "Hub"
  }