"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const devices_1 = __importDefault(require("./api/devices"));
const routes = (0, express_1.Router)();
routes.use('/devices', devices_1.default);
exports.default = routes;
