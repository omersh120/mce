"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findAll = exports.stopMonitoring = exports.startMonitoring = void 0;
const usb_detection_1 = __importDefault(require("usb-detection"));
// Unused service, example to service to works with TCP
const startMonitoring = () => usb_detection_1.default.startMonitoring();
exports.startMonitoring = startMonitoring;
const stopMonitoring = () => usb_detection_1.default.stopMonitoring();
exports.stopMonitoring = stopMonitoring;
const findAll = () => __awaiter(void 0, void 0, void 0, function* () { return usb_detection_1.default.find(); });
exports.findAll = findAll;
