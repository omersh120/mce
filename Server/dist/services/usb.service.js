"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findAll = void 0;
const usb_1 = require("usb");
const usb_device_model_1 = require("../models/usb-device.model");
const device_transformation_service_1 = require("./device-transformation.service");
const findAll = () => {
    let result = [];
    let devices = (0, usb_1.getDeviceList)();
    for (const device of devices) {
        if (device.parent) {
            let child = (0, device_transformation_service_1.transform)(device, usb_device_model_1.USBType.device, []);
            let parent = result.filter((x) => x.productId == device.parent.deviceDescriptor.idProduct
                && x.vendorId == device.parent.deviceDescriptor.idVendor);
            if ((parent === null || parent === void 0 ? void 0 : parent.length) > 0) {
                parent[0].devices.push(child);
            }
            else {
                let transformetedParent = (0, device_transformation_service_1.transform)(device.parent, usb_device_model_1.USBType.hub, [child]);
                result.push(transformetedParent);
            }
        }
    }
    return result;
};
exports.findAll = findAll;
