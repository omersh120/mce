"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.transform = void 0;
const transform = (device, type, devices) => {
    return {
        type: type,
        devices: devices,
        vendorId: device.deviceDescriptor.idVendor,
        productId: device.deviceDescriptor.idProduct,
    };
};
exports.transform = transform;
