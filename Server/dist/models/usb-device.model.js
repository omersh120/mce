"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.USBType = void 0;
var USBType;
(function (USBType) {
    USBType["device"] = "Device";
    USBType["hub"] = "Hub";
})(USBType = exports.USBType || (exports.USBType = {}));
