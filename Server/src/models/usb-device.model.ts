export interface UsbDevice {
    vendorId: number;
    productId: number;
    type: USBType;
    devices: UsbDevice[];
  }

  export enum USBType {
    device = "Device",
    hub = "Hub"
  }