import { Router } from 'express';
import { UsbDevice } from '../../models/usb-device.model';
import { findAll} from '../../services/usb.service';

const devicesRouter = Router();

devicesRouter.get('/', async (request, response) => {
    let result: UsbDevice[] = [];
    try {
        result = await findAll();
    } catch (error) {
        console.log(`Error to get all devices:  ${error}`);
        response.status(500).send('Error! can not get all devices');
    }
    return response.json(result);
});

export default devicesRouter;