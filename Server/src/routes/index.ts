import { Router } from 'express';
import devicesRouter from './api/devices';

const routes = Router();

routes.use('/devices', devicesRouter);

export default routes;