import express, { Express } from 'express';
import routes from './routes';
import cors from 'cors';

const app: Express = express();
const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 3001;

app.use(cors());
app.use(express.json());
app.use('/api', routes);

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://${host}:${port}`);
});

