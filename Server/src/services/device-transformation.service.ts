import { UsbDevice, USBType } from '../models/usb-device.model';

export const transform = (device: any, type: USBType, devices: UsbDevice[]): UsbDevice =>{
    return {
        type: type,
        devices: devices,
        vendorId: device.deviceDescriptor.idVendor,
        productId: device.deviceDescriptor.idProduct,
    };
}

