import usbDetect, { Device } from 'usb-detection';

// Unused service, example to service to works with TCP
export const startMonitoring = () => usbDetect.startMonitoring();
export const stopMonitoring = () => usbDetect.stopMonitoring();
export const findAll = async (): Promise<Device[]> => usbDetect.find();
