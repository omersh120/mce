import { getDeviceList } from 'usb';
import { UsbDevice, USBType } from '../models/usb-device.model';
import { transform } from './device-transformation.service';

export const findAll= (): (UsbDevice[]) => {
    let result: UsbDevice[] = [];
    let devices = getDeviceList();
    for (const device of devices) {
        if (device.parent){
            let child = transform(device, USBType.device, []);
            let parent = result.filter((x: UsbDevice) => x.productId == device.parent.deviceDescriptor.idProduct
                                                        && x.vendorId == device.parent.deviceDescriptor.idVendor);
            if (parent?.length > 0){
                parent[0].devices.push(child);
            }else{
                let transformetedParent = transform(device.parent, USBType.hub, [child])
                result.push(transformetedParent);
            }
        }
    }

    return result;
};
